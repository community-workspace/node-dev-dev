const API_KEY = 'dd44c7c099dfc2248ed82cafccfcd215-0677517f-ec64647f';
const DOMAIN = 'sandbox0fa4c3e252614ee1a5441a929047ee79.mailgun.org';

const formData = require('form-data');
const Mailgun = require('mailgun.js');

const mailgun = new Mailgun(formData);
const client = mailgun.client({username: 'api', key: API_KEY});

let MailClient = {

    async send(content) {

        client.messages.create(DOMAIN, content)
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.error(err);
            });
    }
}
module.exports = {MailClient};