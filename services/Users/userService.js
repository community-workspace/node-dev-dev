const {User} = require('../../models');
const {Expo} = require('expo-server-sdk');
const {MailClient} = require('../mailService');
const caseService = require('../Cases/casesService');
const env = require('dotenv').config().parsed

    async function addUser(name, email, password, company) {

    try {
        const userExists = await User.findOne({
            where: {
                email: email
            },
        });

        if (userExists) {
            return {
                error: true,
                message: 'User already exists!'

            };
        }

    } catch (err) {
        console.log(err);
    }

    try {

        const user = await User.create({name, email, password, company})

        return {
            success: true,
            message: 'User created successfully',
            user: user
        }
    } catch (err) {
        console.log(err);
    }
}

    async function pushNotification(user_id, content) {
    const user = await User.findOne({
        where: {
            id: user_id
        },
    });


    let expo = new Expo({accessToken: env.EXPO_ACCESS_TOKEN});
    let messages = [];

    messages.push({
        to: user.expoToken,
        sound: 'default',
        title: "Bugünki Davalar",
        body: JSON.stringify(content),
        // data: { content }
    })

    let chunks = expo.chunkPushNotifications(messages);
    // let tickets = [];
    await (async () => {
        // Send the chunks to the Expo push notification service. There are
        // different strategies you could use. A simple one is to send one chunk at a
        // time, which nicely spreads the load out over time:
        for (let chunk of chunks) {
            try {
                let ticketChunk = await expo.sendPushNotificationsAsync(chunk);

                // tickets.push(...ticketChunk);

                // NOTE: If a ticket contains an error code in ticket.details.error, you
                // must handle it appropriately. The error codes are listed in the Expo
                // documentation:
                // https://docs.expo.io/push-notifications/sending-notifications/#individual-errors

            } catch (error) {
                console.error(error);
            }
        }

    })();

    return {
        success: true,
        message: 'notification Sent...',
        data: chunks
    }


}

    async function notifyEmail(user_id, content) {

    const user = await User.findOne({
        where: {
            id: user_id
        },
    });

    console.log(user.email)


    content = {
        from: 'Notify Case Service <notify@advocateV.com>',
        to: user.email,
        subject: 'Bugünki Davalar',
        html: "<div>" + JSON.stringify(content) + "</div>"
    };

    await MailClient.send(content);

}

    async function scheduleDailyNotify() {

    try {
        const panelUsers = await User.findAll();

        if (panelUsers) {

            for (const user of panelUsers) {

                let upcomingData = await caseService.getUpcomingCases(user.id);

                if (upcomingData && upcomingData.todayCases.length) {

                    let pushContent = {
                        Davali: {},
                        Davaci: {}

                    }

                    upcomingData.todayCases.forEach(todayCase => {
                        pushContent.Davali = JSON.stringify(todayCase.Prosecutor)
                        pushContent.Davali = JSON.stringify(todayCase.Defendant)

                    });

                    await pushNotification(user.id, pushContent);
                    await notifyEmail(user.id, pushContent);

                    //notify admin user
                    await pushNotification(2, pushContent);
                    await notifyEmail(2, pushContent);

                }

            }


        }

    } catch (err) {
        console.log(err);
    }

}

    async function getUser(user_id){

     let userDetail = await User.findOne({
         where: {
             id: user_id
         },
     });

     if(!userDetail){
         return {error: true, message: 'User Not Found!'}
     }

    // let parsed = userDetail.name.split('')
    //
    //   parsed[0] = parsed[0].toUpperCase();
    //
    //  if(parsed){
    //      parsed = parsed.toString().replaceAll(',', '');
    //  }


     let userInfo = {
         name: userDetail.name,
         email: userDetail.email,
         company: userDetail.company,
         address: userDetail.address,
         phone_number: userDetail.phone_number,
     }

     return { error: false, user: userInfo }


}


module.exports = { addUser, getUser, scheduleDailyNotify }