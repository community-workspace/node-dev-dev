const {Case, Prosecutor, Defendant, CaseDate} = require("../../models");
const moment = require('moment');
const {Op} = require("sequelize");

async function updateCase(case_data, userId) {

    try {

        await Case.update({
            status: 1,
            location: case_data.location,
            case_no: case_data.case_no,
            case_date: case_data.case_date,
            dava_mevzu: case_data.dava_mevzu,
            case_subject: case_data.case_subject,
            gundem: case_data.gundem,
            sira_no: case_data.sira_no,
            description: case_data.description,
        }, {
            where: {
                id: case_data.id,
                UserId: userId
            },

        });

        await Prosecutor.update({
            name: case_data.Prosecutor.name,
            email: case_data.Prosecutor.email,
            advocate: case_data.Prosecutor.advocate,
            phone_number: case_data.Prosecutor.phone_number,
            address: case_data.Prosecutor.address,
        }, {
            where: {
                CaseId: case_data.id,
            },

        });

        await Defendant.update({
            name: case_data.Defendant.name,
            email: case_data.Defendant.email,
            advocate: case_data.Defendant.advocate,
            phone_number: case_data.Defendant.phone_number,
            address: case_data.Defendant.address,
            hukum: case_data.Defendant.hukum,
            icra: case_data.Defendant.icra,
            hapislik: case_data.Defendant.hapislik,
            taksit_orani: case_data.Defendant.taksit_orani,
        }, {
            where: {
                CaseId: case_data.id,
            },

        });


        // await CaseDate.update({
        //     status: 1,
        //     case_date: case_data.case_date
        // }, {
        //     where: {
        //         CaseId: case_data.id,
        //     },
        //
        // });

        return await Case.findOne({
            where: {
                id: case_data.id,
                UserId: userId,
            },
            include: [CaseDate, Defendant, Prosecutor]
        });

    } catch (err) {
        console.log(err);
    }
}

async function getUpcomingCases(user_id) {

    const response = {};
    const dateTomorrow = moment().startOf('day').add(1, 'day').set({hour: 3, minute: 0, second: 0, millisecond: 0});
    const dateToday = moment().set({hour: 3, minute: 0, second: 0, millisecond: 0});

    //Cases Today
    try {
        response.todayCases = await Case.findAll({

            where: {
                UserId: user_id,
            },
            order:  [
                ['createdAt', 'desc']
            ],
            include: [
                {
                    model: CaseDate,
                    where: {
                        case_date: {
                            [Op.gt]: dateToday,
                            [Op.lt]: dateTomorrow,
                        }
                    }
                },
                Defendant,
                Prosecutor
            ]

        });

    } catch (error) {
        console.log(error);
    }

    //Cases Coming Soon
    try {
        response.upcomingCases = await Case.findAll({

            where: {
                UserId: user_id,
            },
            order:  [
                ['createdAt', 'desc']
            ],
            include: [
                {
                model: CaseDate,
                where: {
                    case_date: {
                        [Op.gt]: dateTomorrow,
                        [Op.lt]: moment().add(3, 'day'),
                    }
                }
            },
                Defendant,
                Prosecutor]

        });

    } catch (error) {
        console.log(error);
    }

    return response;
}


async function addNewDate(case_data){

    return await CaseDate.create({
        status: 1,
        case_date: case_data.case_date,
        CaseId: case_data.caseId
    }, {
        where: {
            CaseId: case_data.caseId,
        },

    });
}

module.exports = {updateCase, getUpcomingCases, addNewDate}