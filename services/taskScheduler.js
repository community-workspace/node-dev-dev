const userService = require('../services/Users/userService')

const TaskScheduler = {

    async run() {
        await userService.scheduleDailyNotify();
    }

}

module.exports = {TaskScheduler}