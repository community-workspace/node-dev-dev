'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Case extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    static associate({Defendant, Prosecutor, CaseDate}) {
        // this.belongsTo(User,{foreignKey:'UserId'});
        this.hasOne(Defendant,{ onUpdate:'Cascade',foreignKey:{allowNull:false}});
        this.hasOne(Prosecutor,{ onUpdate:'Cascade',foreignKey:{allowNull:false}});
        this.hasMany(CaseDate,{ onUpdate:'Cascade',foreignKey:{allowNull:false}});
    }
  };

  Case.init({
    status: DataTypes.INTEGER,
    location: DataTypes.STRING,
    case_no: DataTypes.STRING,
    defendant_id: DataTypes.INTEGER,
    prosecutor_id: DataTypes.INTEGER,
    case_date: DataTypes.DATE,
    dava_mevzu: DataTypes.STRING,
    case_subject: DataTypes.STRING,
    gundem: DataTypes.STRING,
    description: DataTypes.STRING,
    sira_no: DataTypes.INTEGER,
    createdAt: {
      type: "TIMESTAMP",
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: "TIMESTAMP",
      defaultValue: sequelize.literal(
          "CURRENT_TIMESTAMP"
      ),
      allowNull: false,
    },
    deletedAt: {
      type: "TIMESTAMP",
    }
  }, {
    sequelize,
    modelName: 'Case',
    timestamps: true,
    paranoid: true,
  });
  return Case;
};