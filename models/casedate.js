'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CaseDate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Case}) {
      this.belongsTo(Case,{onUpdate:'Cascade',foreignKey:{allowNull:false, name: 'CaseId' }});
    }
  }
  CaseDate.init({
    CaseId: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    case_date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'CaseDate',
  });
  return CaseDate;
};