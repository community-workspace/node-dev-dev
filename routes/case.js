var express = require('express');
var router = express.Router();
const moment = require('moment');

const { Case, CaseDate, Defendant, Prosecutor } = require('../models');
const caseService = require('../services/Cases/casesService')

/* GET users listing. */
router.post('/', function (req, res, next) {
    // res.send('user route');
    //   const {name,email} = req.body.params;
    // console.log(name,email);
    // console.log(req.body.params);
});

router.get('/all', async function (req, res, next) {
    try {
        const cases = await Case.findAll({
            where: {
                status: 1,
                UserId: req.user.user_id,
            },
            order:  [
                ['createdAt', 'desc']
            ],
            include: [CaseDate, Defendant, Prosecutor]
        });
        res.send(cases);
    } catch (err) {
        console.log(err);
    }
});

router.post('/create', async function (req, res, next) {

    const {case_data} = req.body.params;

    try {

        const relatedCase = await Case.create({

            status: 1,
            location: case_data.location,
            case_no: case_data.case_no,
            case_date: case_data.case_date,
            dava_mevzu: case_data.dava_mevzu,
            case_subject: case_data.case_subject,
            gundem: case_data.gundem,
            sira_no: case_data.sira_no,
            description: case_data.description,
            UserId: req.user.user_id,

            Prosecutor: {
                name: case_data.prosecutor.name,
                email: case_data.prosecutor.email,
                advocate: case_data.prosecutor.advocate,
                phone_number: case_data.prosecutor.phone_number,
                address: case_data.prosecutor.address,
            },

            Defendant: {
                name: case_data.defendant.name,
                email: case_data.defendant.email,
                advocate: case_data.defendant.advocate,
                phone_number: case_data.defendant.phone_number,
                address: case_data.defendant.address,
                hukum: case_data.defendant.hukum,
                icra: case_data.defendant.icra,
                hapislik: case_data.defendant.hapislik,
                taksit_orani: case_data.defendant.taksit_orani,
            },

        }, {
            include: [CaseDate, Defendant, Prosecutor]
        });

        await CaseDate.create({
            status: 1,
            case_date: case_data.case_date,
            CaseId: relatedCase.id
        }, {
            where: {
                CaseId: relatedCase.id,
            },

        });

        res.send(relatedCase);

    } catch (err) {
        console.log(err);
    }

    // res.send(user);
});

router.post('/update', async function (req, res, next) {

    const {case_data} = req.body.params;
    const userId = req.user.user_id;

    const updatedData = await caseService.updateCase(case_data, userId);

    res.send(updatedData);
});

router.get('/detail/:id', async function (req, res, next) {

    const caseId = req.params.id;
    try {
        const caseDetail = await Case.findOne({
            where: {
                id: caseId,
                UserId: req.user.user_id,
            },
            include: [CaseDate, Defendant, Prosecutor]
        });

        res.send(caseDetail);

    } catch (error) {
        console.log(error);
    }

});

router.get('/upcoming', async function (req, res, next) {

    let upcomingData = await caseService.getUpcomingCases(req.user.user_id);

    res.send(upcomingData);

});

router.post('/addNewDate', async function (req, res, next) {

    let caseDateAdded = await caseService.addNewDate(req.body);

    res.send(caseDateAdded);

});

router.delete('/delete', async function (req, res, next) {

    const caseId = req.params.id;

    await Case.destroy({
        where: {
            id: caseId,
            UserId: req.user.user_id,
        }
    });
});

module.exports = router;

