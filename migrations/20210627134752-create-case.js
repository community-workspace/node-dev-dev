'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('Cases', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            UserId: {
                type: Sequelize.INTEGER(11),
                references: {
                    model: {
                        tableName: 'Users'
                    },
                    key: 'id'
                },
            },
            status: {
                type: Sequelize.INTEGER
            },
            defendant_id: {
                type: Sequelize.INTEGER
            },
            prosecutor_id: {
                type: Sequelize.INTEGER
            },
            case_no: {
                type: Sequelize.INTEGER
            },
            location: {
                type: Sequelize.STRING
            },
            case_date: {
                type: Sequelize.DATE
            },
            dava_mevzu: {
                type: Sequelize.STRING
            },
            case_subject: {
                type: Sequelize.STRING
            },
            gundem: {
                type: Sequelize.STRING
            },
            description: {
                type: Sequelize.STRING
            },
            sira_no: {
                type: Sequelize.INTEGER
            },
            createdAt: {
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
                allowNull: false,
            },
            updatedAt: {
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal(
                    "CURRENT_TIMESTAMP"
                ),
                allowNull: false,
            },
            deletedAt: {
                type: "TIMESTAMP",
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Cases');
    }
};