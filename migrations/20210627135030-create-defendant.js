'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('Defendants', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
          CaseId: {
            type: Sequelize.INTEGER(11),
            references: {
              model: {
                tableName: 'Cases'
              },
              key: 'id'
            },
          },
            name: {
                type: Sequelize.STRING
            },
            email: {
                type: Sequelize.STRING
            },
            advocate: {
                type: Sequelize.STRING
            },
            phone_number: {
                type: Sequelize.STRING
            },
            address: {
                type: Sequelize.STRING
            },
            hukum: {
                type: Sequelize.STRING
            },
            icra: {
                type: Sequelize.STRING
            },
            hapislik: {
                type: Sequelize.STRING
            },
            taksit_orani: {
                type: Sequelize.STRING
            },
          createdAt: {
            type: "TIMESTAMP",
            defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
            allowNull: false,
          },
          updatedAt: {
            type: "TIMESTAMP",
            defaultValue: Sequelize.literal(
                "CURRENT_TIMESTAMP"
            ),
            allowNull: false,
          },
          deletedAt: {
            type: "TIMESTAMP",
          }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Defendants');
    }
};